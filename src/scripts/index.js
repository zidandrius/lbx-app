'use strict';

var siteCode = function MainFn() {
  var homeForm = document.getElementById('home-from');
  var homeFormButton = homeForm.querySelector('.btn--primary');
  var resultsTable = document.getElementById('results-table');

  var submitHandler = function submitHandlerFn() {
    homeFormButton.setAttribute('disabled', 'disabled');

    setTimeout(function formSubmitResponseFn() {
      var successMessage = document.createTextNode('Спасибо за регистрацию');
      var h3 = document.createElement('h3');
      h3.appendChild(successMessage);
      h3.className += 'success-message';
      homeFormButton.remove();
      homeForm.appendChild(h3);
    }, 2000);
  };

  var renderTable = function renderTableFn(values) {
    var i;
    var j;
    var tr;
    var td;
    var txt;
    var div;
    var span;
    var tbody = document.createElement('tbody');

    for (i = 0; i < values.length; i += 1) {
      tr = document.createElement('tr');

      for (j = 0; j < values[i].length; j += 1) {
        td = document.createElement('td');
        txt = document.createTextNode(values[i][j]);

        if (j === 0) {
          div = document.createElement('div');
          div.className += 'results__row-background';
          td.appendChild(div);

          span = document.createElement('span');
          span.className += 'results__place-text';
          span.appendChild(txt);
          td.appendChild(span);

          td.className += 'results__place-column';
          tr.appendChild(td);

          if (values[i][j] < 4) {
            tr.className += 'results__place-row--best';
          }
        } else {
          if (j === 3) {
            span = document.createElement('span');
            span.className += 'flag-icon flag-icon-' + values[i][j];
            td.className += 'right';
            td.appendChild(span);
          } else {
            td.appendChild(txt);
          }

          tr.appendChild(td);
        }
      }

      tbody.appendChild(tr);
    }

    resultsTable.querySelector('tbody').remove();
    resultsTable.appendChild(tbody);
  };

  var tableData = [
    ['1', '$5 321', '123 456 000', 'ru'],
    ['2', '$3 000', '425 238 238', 'es'],
    ['4', '$777', '123 543 548', 'cn'],
    ['3', '$890', '090 235 453', 'ru'],
    ['5', '$666', '009 281 443', 'es'],
    ['6', '$555', '515 654 522', 'cn'],
    ['7', '$444', '765 452 241', 'ru'],
    ['8', '$333', '562 183 816', 'es'],
    ['9', '$222', '652 452 555', 'cn'],
    ['10', '$111', '212 552 112', 'ru']
  ];

  var sortTableData = function sortTableDataFn(data, column, order) {
    resultsTable.querySelectorAll('th').forEach(function (th) {
      th.removeAttribute('aria-sort');

      if (th.cellIndex === column) {
        th.setAttribute('aria-sort', order === 'asc' ? 'ascending' : 'decending');
      }
    });

    data.sort(function sortFn(a, b) {
      var valueA = parseInt(a[column].replace(' ', '').replace('$', ''), 10);
      var valueB = parseInt(b[column].replace(' ', '').replace('$', ''), 10);
      return order === 'asc' ? valueA - valueB : valueB - valueA;
    });
  };

  homeForm.addEventListener('submit', submitHandler);
  homeFormButton.removeAttribute('disabled');
  sortTableData(tableData, 0, 'asc');
  renderTable(tableData);
  resultsTable.querySelectorAll('th').forEach(function (th) {
    if (th.cellIndex === 3) {
        return;
    }
    th.className += ' sortable';
    th.addEventListener('click', function (e) {
      var sortOrder = e.target.getAttribute('aria-sort') === 'ascending' ? 'desc' : 'asc';
      sortTableData(tableData, e.target.cellIndex, sortOrder);
      renderTable(tableData);
    })
  });
};

if (document.getElementById('home-from')) {
  siteCode();
} else {
  document.addEventListener('DOMContentLoaded', function DOMContentLoadedFn() {
    siteCode();
  });
}
