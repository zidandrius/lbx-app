/* eslint-disable func-names */

var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var lesshint = require('gulp-lesshint');
var connect = require('gulp-connect');

// compile css
gulp.task('lint-less', function () {
  return gulp.src('./src/less/**/*.less')
    .pipe(lesshint({ zeroUnit: false }))
    .pipe(lesshint.reporter())
    .pipe(lesshint.failOnError());
});

gulp.task('less', function () {
  return gulp
    .src('./src/less/styles.less')
    .pipe(
      less({
        paths: [path.join(__dirname, 'less', 'includes')]
      })
    )
    .pipe(gulp.dest('./dist/css'))
    .pipe(connect.reload());
});

gulp.task('watch-less', function () {
  gulp.watch('src/less/**/*.less', gulp.series('lint-less', 'less'));
});

// compile html
gulp.task('html', function () {
  return gulp.src('src/templates/**/*.html')
    .pipe(gulp.dest('dist'))
    .pipe(connect.reload());
});

gulp.task('watch-html', function () {
  gulp.watch('src/templates/**/*.html', gulp.registry().get('html'));
});

// optimize images
gulp.task('assets', function () {
  return gulp.src('src/assets/**/*.+(jpg|svg|png)')
    .pipe(gulp.dest('dist/img'));
});

gulp.task('flags', function () {
  return gulp.src('src/flags/**/*.+(jpg|svg|png)')
    .pipe(gulp.dest('dist/flags'));
});

// bunde js
gulp.task('js', function () {
  return gulp.src('src/scripts/index.js')
    .pipe(gulp.dest('dist/js'));
});

gulp.task('watch-js', function () {
  gulp.watch('src/scripts/**/*.js', gulp.registry().get('js'));
});

// main tasks
gulp.task('dev-server', function () {
  connect.server({
    root: './dist',
    livereload: true
  });
});

gulp.task('build', gulp.series('lint-less', 'less', 'html', 'js', 'assets', 'flags'));

gulp.task('dev', gulp.parallel('dev-server', 'watch-html', 'watch-less', 'watch-js'));
